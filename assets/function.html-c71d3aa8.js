import{_ as n,p as a,q as s,a1 as e}from"./framework-61fce562.js";const i={},c=e(`<h1 id="函数" tabindex="-1"><a class="header-anchor" href="#函数" aria-hidden="true">#</a> 函数</h1><h2 id="定义函数" tabindex="-1"><a class="header-anchor" href="#定义函数" aria-hidden="true">#</a> 定义函数</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token function-name function">name</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
<span class="token punctuation">{</span>
	command1
	command2
	<span class="token punctuation">..</span>.
	commandn
<span class="token punctuation">}</span>

<span class="token keyword">function</span> <span class="token function-name function">name</span>
<span class="token punctuation">{</span>
	command1
	command2
	<span class="token punctuation">..</span>.
	commandn
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><div class="custom-container tip"><p class="custom-container-title">Tip</p><p><code>$$</code>获取当前执行脚本的pid, 可用于过滤当前脚本进程，grep -v $$</p></div><h2 id="函数返回值" tabindex="-1"><a class="header-anchor" href="#函数返回值" aria-hidden="true">#</a> 函数返回值</h2><p>函数return返回值范围只能是0-255，echo可以返回任何字符串。</p><h2 id="局部变量" tabindex="-1"><a class="header-anchor" href="#局部变量" aria-hidden="true">#</a> 局部变量</h2><p>如果一个变量不特别声明，那么它是全局变量，如果想定义为局部变量需要使用<code>local</code>。</p><h2 id="函数库" tabindex="-1"><a class="header-anchor" href="#函数库" aria-hidden="true">#</a> 函数库</h2><p>通过<code>.</code>引用外部文件。需要通过绝对路径引入，一般库文件后缀是.lib, 库文件通常没有可执行权限，第一行用如下：</p><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token shebang important">#!/bin/echo </span>

<span class="token keyword">function</span> <span class="token function-name function">name</span>
<span class="token punctuation">{</span>

<span class="token punctuation">}</span>
</code></pre><div class="highlight-lines"><div class="highlight-line"> </div><br><br><br><br><br></div><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,11),d=[c];function t(l,o){return a(),s("div",null,d)}const u=n(i,[["render",t],["__file","function.html.vue"]]);export{u as default};
