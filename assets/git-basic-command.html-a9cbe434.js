import{_ as n,p as s,q as a,a1 as e}from"./framework-61fce562.js";const i={},l=e(`<h1 id="git-基本命令" tabindex="-1"><a class="header-anchor" href="#git-基本命令" aria-hidden="true">#</a> git 基本命令</h1><h2 id="工作区管理" tabindex="-1"><a class="header-anchor" href="#工作区管理" aria-hidden="true">#</a> 工作区管理</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment"># 命令用来从工作目录中删除所有没有跟踪（tracked）过的文件</span>
<span class="token function">git</span> clean

<span class="token comment"># 是一次clean的演习, 告诉你哪些文件会被删除</span>
<span class="token function">git</span> clean <span class="token parameter variable">-n</span> 

<span class="token comment"># 删除当前目录下没有tracked过的文件，不会删除.gitignore指定的文件</span>
<span class="token function">git</span> clean <span class="token parameter variable">-f</span> 

<span class="token comment"># 删除当前目录下没有被tracked过的文件和文件夹</span>
<span class="token function">git</span> clean <span class="token parameter variable">-df</span> 

<span class="token comment"># 将没有放入到暂存区的所有文件恢复</span>
<span class="token function">git</span> checkout <span class="token builtin class-name">.</span> 

<span class="token comment">#放弃指定文件的修改</span>
<span class="token function">git</span> checkout <span class="token function">file</span> 

<span class="token comment"># 暂存现在工作区的工作，然后恢复原来为编辑的状态，但是不包括未被跟踪的文件，也就是新建的文件</span>
<span class="token function">git</span> stash 

<span class="token comment"># 显示现在正在暂存的工作</span>
<span class="token function">git</span> stash list

<span class="token comment"># 恢复stash暂存区的工作</span>
<span class="token function">git</span> stash apply stash@<span class="token punctuation">{</span><span class="token number">0</span><span class="token punctuation">}</span> 
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="暂存区管理" tabindex="-1"><a class="header-anchor" href="#暂存区管理" aria-hidden="true">#</a> 暂存区管理</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment"># 提交所有修改和新增的文件</span>
<span class="token function">git</span> <span class="token function">add</span> <span class="token builtin class-name">.</span> 
<span class="token function">git</span> <span class="token function">add</span> <span class="token parameter variable">-u</span> 

<span class="token comment"># 撤销暂存区的所有文件</span>
<span class="token function">git</span> reset HEAD  

<span class="token comment"># 撤销暂存区的指定文件</span>
<span class="token function">git</span> reset HEAD <span class="token function">file</span> 

<span class="token comment"># 查看暂存区文件列表</span>
<span class="token function">git</span> ls-files <span class="token parameter variable">-s</span> 

<span class="token comment"># 查看暂存区文件内容</span>
<span class="token function">git</span> cat-file <span class="token parameter variable">-p</span> 6e9a94 

<span class="token comment"># 提交一次版本</span>
<span class="token function">git</span> commit <span class="token parameter variable">-m</span> ‘提交信息’ 

<span class="token comment"># 提交一次版本， 同git add . &amp;&amp; git commit -m &quot;message&quot;</span>
<span class="token function">git</span> commit <span class="token parameter variable">-am</span> ‘提交信息’ 

<span class="token comment"># 将误删除文件从上次版本库中恢复</span>
<span class="token function">git</span> checkout HEAD -- deletedFile 

<span class="token comment"># 将误删除文件从上上次提交的版本库中恢复</span>
<span class="token function">git</span> checkout HEAD^ -- deletedFile 

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="版本库管理" tabindex="-1"><a class="header-anchor" href="#版本库管理" aria-hidden="true">#</a> 版本库管理</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment">#使用reset恢复到历史提交点，重置暂存区与工作目录的内容。</span>

<span class="token comment"># 保留工作区的内容，不保留暂存区 等同 git reset --mixed</span>
<span class="token function">git</span> reset 

<span class="token comment"># 保留工作区的内容，把文件差异放进暂存区，即保留暂存区</span>
<span class="token function">git</span> reset <span class="token parameter variable">--soft</span> 

<span class="token comment"># 清空工作区和暂存区的改动</span>
<span class="token function">git</span> reset <span class="token parameter variable">--hard</span> 

<span class="token comment"># 恢复前三个版本</span>
<span class="token function">git</span> reset <span class="token parameter variable">--hard</span> HEAD^^^ 

<span class="token comment"># (同上)恢复前三个版本</span>
<span class="token function">git</span> reset <span class="token parameter variable">--hard</span> HEAD~3 

<span class="token comment"># 恢复到指定提交版本（先通过 git log 查看版本号)</span>
<span class="token function">git</span> reset <span class="token parameter variable">--hard</span> b7b73147ca8d6fc20e451d7b36 

<span class="token comment"># 放弃已经add 暂存区的文件hd.js</span>
<span class="token function">git</span> reset HEAD hd.js 

<span class="token comment"># 可以查询之前进行的 每一次 git 命令，即包括reset前的所有日志，即显示每一步操作信息</span>
<span class="token function">git</span> reflog 

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="分支管理-branch" tabindex="-1"><a class="header-anchor" href="#分支管理-branch" aria-hidden="true">#</a> 分支管理 branch</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment">#分支用于为项目增加新功能或修复Bug时使用。</span>

<span class="token comment"># 创建分支</span>
<span class="token function">git</span> branch dev

<span class="token comment"># 查看分支 </span>
<span class="token function">git</span> branch

<span class="token comment"># 切换分支 </span>
<span class="token function">git</span> checkout dev

<span class="token comment"># 创建并切换分支 </span>
<span class="token function">git</span> checkout <span class="token parameter variable">-b</span> feature/bbs

<span class="token comment"># 查看远程分支</span>
<span class="token function">git</span> branch <span class="token parameter variable">-r</span> 

<span class="token comment"># 将分支 main 更新为master </span>
<span class="token function">git</span> branch <span class="token parameter variable">-m</span> main master

<span class="token comment"># 合并dev分支到master </span>
<span class="token function">git</span> checkout master
<span class="token function">git</span> merge dev

<span class="token comment"># 删除分支 </span>
<span class="token function">git</span> branch <span class="token parameter variable">-d</span> dev

<span class="token comment"># 删除没有合并的分支</span>
<span class="token function">git</span> branch <span class="token parameter variable">-D</span> dev

<span class="token comment"># 删除远程分支 </span>
<span class="token function">git</span> push origin :dev

<span class="token comment"># 查看未合并的分支(切换到master) </span>
<span class="token function">git</span> branch --no-merged

<span class="token comment"># 查看已经合并的分支(切换到master) </span>
<span class="token function">git</span> branch <span class="token parameter variable">--merged</span>

<span class="token comment"># 查看两个分支的变动</span>
<span class="token function">git</span> <span class="token function">diff</span> branch-a<span class="token punctuation">..</span>branch-b
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="日志查看-log" tabindex="-1"><a class="header-anchor" href="#日志查看-log" aria-hidden="true">#</a> 日志查看 log</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment">#查看日志 </span>
<span class="token function">git</span> log

<span class="token comment"># 查看最近2次提交日志并显示文件差异 </span>
<span class="token function">git</span> log <span class="token parameter variable">-p</span> <span class="token parameter variable">-2</span>

<span class="token comment"># 显示已修改的文件清单 </span>
<span class="token function">git</span> log --name-only

<span class="token comment"># 显示新增、修改、删除的文件清单 </span>
<span class="token function">git</span> log --name-status

<span class="token comment"># 一行显示并只显示SHA-1的前几个字符 </span>
<span class="token function">git</span> log <span class="token parameter variable">--oneline</span>

<span class="token comment"># 一行显示并只显示SHA-1的前几个字符及最近的5条信息</span>
<span class="token function">git</span> log <span class="token parameter variable">--oneline</span> <span class="token parameter variable">-5</span>

<span class="token comment"># 过滤日志信息</span>
<span class="token function">git</span> log <span class="token parameter variable">--oneline</span> <span class="token parameter variable">--grep</span><span class="token operator">=</span><span class="token string">&quot;filer info&quot;</span>

<span class="token comment"># 查看特定时间段日志</span>
<span class="token function">git</span> log <span class="token parameter variable">--before</span><span class="token operator">=</span><span class="token string">&#39;1 day&#39;</span>
<span class="token function">git</span> log <span class="token parameter variable">--after</span><span class="token operator">=</span><span class="token string">&#39;1 day&#39;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="远程管理-git-remote" tabindex="-1"><a class="header-anchor" href="#远程管理-git-remote" aria-hidden="true">#</a> 远程管理 git remote</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment"># 添加远程分支</span>
<span class="token function">git</span> remote <span class="token function">add</span> origin https://gitee.com/loveagri/blank.git

<span class="token comment"># 查看远程分支</span>
<span class="token function">git</span> branch <span class="token parameter variable">-r</span> 

<span class="token comment"># 查看远程分支关联地址</span>
<span class="token comment"># origin  git@gitee.com:loveagri/blank.git (fetch)</span>
<span class="token comment"># origin  git@gitee.com:loveagri/blank.git (push)</span>
<span class="token function">git</span> remote <span class="token parameter variable">-v</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="推送分支到远程-git-push" tabindex="-1"><a class="header-anchor" href="#推送分支到远程-git-push" aria-hidden="true">#</a> 推送分支到远程 git push</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment"># todo</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><h2 id="远程拉取-git-pull-git-fetch-git-merge" tabindex="-1"><a class="header-anchor" href="#远程拉取-git-pull-git-fetch-git-merge" aria-hidden="true">#</a> 远程拉取 git pull, git fetch, git merge</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment"># 拉取远程分支但是不合并</span>
<span class="token function">git</span> fetch

<span class="token comment"># 合并远程分支</span>
<span class="token function">git</span> merge

<span class="token comment">#直接拉取远程分支</span>
<span class="token function">git</span> pull
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="git-diff" tabindex="-1"><a class="header-anchor" href="#git-diff" aria-hidden="true">#</a> git diff</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment"># 查看两个分支的变动</span>
<span class="token function">git</span> <span class="token function">diff</span> branch-a<span class="token punctuation">..</span>branch-b
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div>`,19),c=[l];function t(d,r){return s(),a("div",null,c)}const p=n(i,[["render",t],["__file","git-basic-command.html.vue"]]);export{p as default};
