import{_ as r,M as s,p as l,q as d,R as e,t as i,N as o,a1 as a}from"./framework-61fce562.js";const c={},t=a(`<h1 id="laravel模型提示及debugbar使用" tabindex="-1"><a class="header-anchor" href="#laravel模型提示及debugbar使用" aria-hidden="true">#</a> laravel模型提示及Debugbar使用</h1><hr><h3 id="生成模型提示" tabindex="-1"><a class="header-anchor" href="#生成模型提示" aria-hidden="true">#</a> 生成模型提示</h3><hr><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment"># composer 安装</span>
<span class="token function">composer</span> require barryvdh/laravel-ide-helper <span class="token parameter variable">--dev</span>

<span class="token comment"># 生成模型的工具：</span>
php artisan ide-helper:models

<span class="token comment"># 生成_ide_helper.php文件 外观方法的代码提示：</span>
php artisan ide-helper:generate

<span class="token comment"># 通过容器调用的类的代码提示：</span>
php artisan ide-helper:meta
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="laravel框架开发调试工具laravel-debugbar使用" tabindex="-1"><a class="header-anchor" href="#laravel框架开发调试工具laravel-debugbar使用" aria-hidden="true">#</a> Laravel框架开发调试工具Laravel Debugbar使用</h3><hr>`,7),p={href:"https://www.dandelioncloud.cn/article/details/1531824649600266241",target:"_blank",rel:"noopener noreferrer"},v=a(`<ol><li>使用 Composer 安装该扩展包：</li></ol><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token function">composer</span> require barryvdh/laravel-debugbar <span class="token parameter variable">--dev</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><ol start="2"><li><p>安装完成后，修改 <code>config/app.php</code> 在 <code>providers</code> 数组内追加 Debugbar 的 Provider</p></li><li><p>关闭debugbar需要去.env里把<code>APP_DEBUG</code>设置为<code>false</code></p></li></ol>`,3);function b(h,u){const n=s("ExternalLinkIcon");return l(),d("div",null,[t,e("p",null,[e("a",p,[i("原文链接"),o(n)])]),v])}const _=r(c,[["render",b],["__file","laravel-model-tool-and-debugbar.html.vue"]]);export{_ as default};
