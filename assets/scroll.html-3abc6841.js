import{_ as e,p as n,q as l,a1 as i}from"./framework-61fce562.js";const d={},s=i(`<h1 id="vue3中监听滚动条事件" tabindex="-1"><a class="header-anchor" href="#vue3中监听滚动条事件" aria-hidden="true">#</a> vue3中监听滚动条事件</h1><div class="language-vue line-numbers-mode" data-ext="vue"><pre class="language-vue"><code>import { onMounted, onUnmounted, reactive } from &#39;vue&#39;
const data=reactive({
    oldScrollTop:0,
});
const scrolling=()=&gt;{
    // 滚动条距文档顶部的距离
    let scrollTop =window.pageYOffset ||document.documentElement.scrollTop ||document.body.scrollTop;
    // 滚动条滚动的距离
    let scrollStep = scrollTop - data.oldScrollTop;
    console.log(&quot;header 滚动距离 &quot;, scrollTop);
    // 更新——滚动前，滚动条距文档顶部的距离
    data.oldScrollTop = scrollTop;
 
    //变量windowHeight是可视区的高度
    let windowHeight =
    document.documentElement.clientHeight || document.body.clientHeight;
    //变量scrollHeight是滚动条的总高度
    let scrollHeight =
    document.documentElement.scrollHeight || document.body.scrollHeight;
 
    //滚动条到底部的条件
    if (scrollTop + windowHeight == scrollHeight) {
    //你想做的事情
        console.log(&quot;header  你已经到底部了&quot;);
    }
    if (scrollStep &lt; 0) {
        console.log(&quot;header 滚动条向上滚动了！&quot;);
    } else {
        console.log(&quot;header  滚动条向下滚动了！&quot;);
    }
    // 判断是否到了最顶部
    if (scrollTop &lt;= 0) {
        console.log(&quot;header 到了最顶部&quot;)
    }; 
};
 
onMounted(()=&gt;{
    window.addEventListener(&quot;scroll&quot;, scrolling);
})
onUnmounted(()=&gt;{
    window.removeEventListener(&quot;scroll&quot;, scrolling);
})
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,2),o=[s];function c(r,v){return n(),l("div",null,o)}const a=e(d,[["render",c],["__file","scroll.html.vue"]]);export{a as default};
