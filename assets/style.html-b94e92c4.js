import{_ as s,p as n,q as a,a1 as e}from"./framework-61fce562.js";const t={},l=e(`<h1 id="样式相关问题" tabindex="-1"><a class="header-anchor" href="#样式相关问题" aria-hidden="true">#</a> 样式相关问题</h1><ol><li><h3 id="css-vue-js-使样式被子组件继承" tabindex="-1"><a class="header-anchor" href="#css-vue-js-使样式被子组件继承" aria-hidden="true">#</a> css - Vue js - 使样式被子组件继承</h3></li></ol><p>当我想为当前组件设置一个私有(private)样式时，我将它定义在一个标签中。 为了使样式对子组件可见，我使用了深度选择器让它通过，如下所示:</p><div class="language-vue line-numbers-mode" data-ext="vue"><pre class="language-vue"><code><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>style</span> <span class="token attr-name">scoped</span><span class="token punctuation">&gt;</span></span><span class="token style"><span class="token language-css">
    <span class="token selector">::v-deep .b</span> <span class="token punctuation">{</span>
        ...
    <span class="token punctuation">}</span>
</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>style</span><span class="token punctuation">&gt;</span></span>

<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>style</span> <span class="token attr-name">scoped</span><span class="token punctuation">&gt;</span></span><span class="token style"><span class="token language-css">
    <span class="token selector">:deep(.b)</span> <span class="token punctuation">{</span>
        ...
    <span class="token punctuation">}</span>
</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>style</span><span class="token punctuation">&gt;</span></span>

//或者去掉scoped
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,4),c=[l];function p(i,o){return n(),a("div",null,c)}const u=s(t,[["render",p],["__file","style.html.vue"]]);export{u as default};
